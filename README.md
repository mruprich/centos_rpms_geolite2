Upstream changed their license on 2019-12-30.  The currently packaged version
(20191217) is the last version released under CC-BY-SA.

[rhbz#1786211](https://bugzilla.redhat.com/show_bug.cgi?id=1786211)
